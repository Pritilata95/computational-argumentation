## Assignment 2: Argument Mining

* Assignment goals  
  * working with publicly available argumentative data
  * characterizing claims and identifying them in texts
  * training a classifier to detect claims automatically
* Tasks
  * ***Feature engineering***: (linguistic) features that help in identifying claims
  * ***Classification***:
    * a classifier that is able to classify claims and non-claims
    * uses the features developed in the Feature Engineering step
  * ***Documentation:*** description of your approach
    * describe **what** you did and **why** it is helpful for this task

### A Look at the Data

* Data and annotations
  * short texts annotated for containing claim/no-claim
  * crawled from the Reddit forum ChangeMyView for various topics
  * manually annotated
* Training Data  
  * 2619 texts in total  
  * 872 claims, 1747 non-claims  
* Validation Data  
  * 349 texts in total  
  * 126 claims, 223 non-claims  
* Test Data  
  * 525 texts in total  
  * 208 claims, 317 non-claims  

### Task: Feature Engineering

* What does Feature Engineering mean?
  * building features that describe (parts of) a text
  * can be utilized by ML models to do computations on texts
* Find a way to model texts that contain claims.
  * identifying certain linguistic properties of those texts
  * examples include: n-grams, part-of-speech, token statistics etc. 
  * can be on the token-, phrase- or sentence-level
* Encode those features into feature vectors
  * those are the input to the classification model

### Task: Classification 

* Choose a ML model
  * some models might make more sense than others
  * examples include: SVM, Random Forest, Neural networks etc.
* Train the ML model
  * train a model to detect texts containing claims
  * each text should be identified by the feature vector you build for it
  * must not be trained on the validation set
* Save predictions
  * your model should save predictions in the following format  
    {  
     &nbsp;&nbsp;&nbsp;&nbsp; "<text_id_1>": "\<label\>",  
     &nbsp;&nbsp;&nbsp;&nbsp; "<text_id_2>": "\<label\>",  
     &nbsp;&nbsp;&nbsp;&nbsp; "<text_id_3>": "\<label\>",  
     &nbsp;&nbsp;&nbsp;&nbsp; ...  
    }  
    where  
      * *<text_id_X>* is the id of the text  
      * *\<label\>* is either 1 (contains a claim) or 0 (contains no claim)  
  * the input to your script will be in the same format as the training/validation data
  
Instructions for execution can be found in **DOCUMENTATION.pdf**
