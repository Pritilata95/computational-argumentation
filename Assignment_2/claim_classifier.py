# -*- coding: utf-8 -*-
"""
@author: Lannisters
"""
import pandas as pd
import spacy
import re
from sklearn.naive_bayes import GaussianNB
from sklearn.feature_extraction.text import TfidfVectorizer
import sys
import json

#sys.executable -m spacy download en_core_web_sm
pd.options.mode.chained_assignment = None 
nlp=spacy.load("en_core_web_sm")
  
def preprocess(rawtext):
    newtext=removeurl(rawtext)
    newtext=removechars(newtext)
    doc=nlp(newtext)
    tokenlist=[]
    for i in doc: 
        if (not i.is_space and not i.is_punct and not i.like_num 
            and i.ent_type == 0 and len(i.text)>2):
            if(len(i.lemma_)>2):
                tokenlist.append(i.lemma_.lower()) 
    return " ".join(tokenlist)

def removeurl(rawtext):
    text=re.sub("(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?","",rawtext)
    text=re.sub("([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)","",text)
    return text

def removechars(rawtext):
    text=rawtext
    text=text.strip()
    text=re.sub("[\[\]]|[^A-z.!?\']"," ",text)
    text=re.sub("[\s]+"," ",text)
    return text

def keep_args(text):
    global commonvocab
    l = text.split()
    for t in l:
        if t in commonvocab:
            l.remove(t)
    return " ".join(l)

def classify(train, test):
    
    data=pd.read_json(train)
    valdata=pd.read_json(test)
    
    for i in range(len(data)):
        data["text"][i]  = preprocess(data["text"][i])
    
    claims = data[data['label'] == 1]
    nonclaims = data[data['label'] == 0]
    claimvocab = set()
    nonclaimvocab = set()
    for text in claims['text']:
        claimvocab.update(text.split())
    for text in nonclaims['text']:
        nonclaimvocab.update(text.split())
    global commonvocab
    commonvocab = claimvocab.intersection(nonclaimvocab)
    common_word_freq = {}
    for v in commonvocab:
        common_word_freq[v] = sum(v in s for s in data['text'])
    common_word_freq=dict(sorted(common_word_freq.items(), key=lambda item: item[1],reverse=True)[:700])
    commonvocab=commonvocab.intersection(common_word_freq.keys())
    
    for i in range(len(data)):
        data["text"][i]  = keep_args(data["text"][i])
    
    for i in range(len(valdata)):
        valdata["text"][i]  = preprocess(valdata["text"][i])
        valdata["text"][i]  = keep_args(valdata["text"][i])  
    
    vectorizer = TfidfVectorizer(ngram_range=(2,3))
    X = vectorizer.fit_transform(data["text"]).toarray()
    y=data["label"]
    Xpred=vectorizer.transform(valdata["text"]).toarray()
    
    nbclf=GaussianNB()
    nbclf.fit(X, y)
    ypred=nbclf.predict(Xpred)
    valdata["predictions"]=ypred
    
    valdata = valdata.drop(['text','label'], axis=1)
    valdata = valdata.set_index('id')
    with open('output.json', 'w', encoding='utf-8') as f:
        json.dump(valdata.to_dict()['predictions'], f, indent=4)
        
    print("Output is written to file output.json")
        
if __name__ == "__main__":
    train_file = sys.argv[1]
    test_file = sys.argv[2]
    classify(train_file, test_file)