## Assignment 4: Argument Generation

### WARNING: THIS ASSIGNMENT WAS EXECUTED IN GOOGLE COLAB, AS IT REQUIRES HIGH GPU PERFORMANCE. WHILE EXECUTING THIS ASSIGNMENT, PLEASE USE GOOGLE COLAB OR ANY HIGH GPU (NEARLY 16 GB) RESOURCES

Assignment goals  
* Learning about argument conclusions  
* Learning about different approaches of generating argumentative texts  
* Learning evaluation measures for text generation approaches  

Tasks  
* **Conclusion generation**: given an argument in natural language text, generate an argumentative statement representing the conclusion of the argument.  
* **Documentation**: Describe your approach for the task, along with the features, models, and optimizations you performed.  

### A look at the data

Collected from online platforms like:  
* Reddit forum ChangeMyView, Kialo platform, Debate.org  

Each instance contains:  
* **Id**: Argument identifier  
* **Conclusion**: The ground-truth conclusion of the argument  
* **Argument**: The text of the argument (representing the premises)  

### Task: Conclusion Generation

* The task is similar to text summarization tasks.  
* The generated conclusion can be:  
  * An extract from the argument  
  * A new utterance that is abstractly generated (inferred) from the argument.  
* Different approaches can be followed:  
  * Graph based approaches  
  * Ranking approaches  
  * Deep Learning approaches  
* Features:  
  * Token embedding, syntactic, structure, etc.  
  
Instructions for execution can be found in **DOCUMENTATION.pdf**
