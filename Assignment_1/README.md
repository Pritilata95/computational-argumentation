## Assignment 1: Data Acquisition

* Assignment goals:  
  * Learning to acquire publicly available texts on debate portals  
  * Extract structured data from these texts  
  * Compute some statistics on your data  
* Tasks:  
  * Data Crawling: five opinion pages from debate.org and save them to a JSON file  
  * Preliminary Statistics: Perform preliminary statistics to learn about the crawled data

### Task: Data Crawling

* Top five popular pages from debate.org  
* For each page:  
  * Topic
  * Category
  * A complete list of pro/con arguments, each containing:  
    * Argument title
    * Argument body
* Save the crawled data into a structured JSON file
* Contains a list of opinion page objects, where each objects consists of:
  * topic: the topic of the page
  * category: the category of the page
  * pro_arguments: list of pro argument objects
  * con_arguments: list of con argument objects

### Task: Preliminary Statistics

* A histogram of argument lengths
* A histogram of number of pro/con argument per topic and category

Instructions for execution can be found in **DOCUMENTATION.pdf**
