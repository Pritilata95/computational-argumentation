## Assignment 3: Argument Assessment

Assignment goals  
* characterizing if a dialogue leads to ad hominem  
* modeling dialogues as sequential information  
* training a classifier to identify ad hominem inducing dialogues  

Tasks
* **Dialogue modeling**: model dialogical/sequential information leading to ad hominem posts  
* **Classification**: a classifier that is able to predict ad hominem attacks given a preceding dialogue  
* **Documentation**: description of your approach  
  * describes and justifies the chosen approach and classifier(s)
  * describes how you model the dialog

### A look at the data

Data and annotations
  * online interactions between users annotated for leading to ad hominem  
  * crawled from the Reddit forum ChangeMyView for various topics  
  * annotated by moderators of the forum for violating the rules  
    * ad hominem → “Rude or hostile” - rule violation  

Each instance contains 2 posts  
  * models the dialog of users before an ad hominem or delta reward
  * positive samples lead to ad hominem posts
  * negative samples lead to delta rewarded (non-ad hominem) posts

Training dataset
  * 1936 threads in total (2 posts each)  
  * 968 for each class  
  
Validation dataset  
  * 258 threads in total (2 posts each)  
  * 129 for each class  
  
Test dataset  
  * 388 threads in total (2 posts each)  
  * 194 for each class  

### Task: Dialogue Modeling

**Sequential information of a dialogue**  
  * what happened before what? what happened after?  

**Basically everything that describes the dialogue**
  * identifying rhetorical devices, such as sarcasm, or other indicators
  * looking for specific keywords in previous posts
  * using sequence-aware (neural) models
  * using the attributes of previous posts, e.g. up-/down-votes
  * …

### Task: Ad hominem prediction

**Given a dialogue, predict if it leads to an ad hominem post**
  * build a classifier able to predict the outcome of a dialog
  * model the three preceding posts and their sequential/dialogical structure

**Features**
  * you are free to choose any features you think are helpful
    * except the *final_post* property!
  * should reflect the interaction between the users/in the dialogue
  * they need to be justified in the documentation

**Classification model**
  * you are free to choose any classifier/model you think makes sense
  * needs to be justified in the documentation

Instructions for execution can be found in **DOCUMENTATION.pdf**
