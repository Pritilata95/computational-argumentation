# -*- coding: utf-8 -*-
"""
Created on Sat Jun 12 13:09:23 2021

@author: Abhirup Sinha
"""

import json
import pandas as pd

with open('train-data-prepared.json') as json_file:
    train_data = json.load(json_file)
    
train = pd.DataFrame()

train_record = []
for record in train_data:
    votes = {'upvotes':0,'downvotes':0}
    delta = 0
    ID = record['id']
    label = record['label']
    convo1 = record['preceding_posts'][0]['body']
    author1 = record['preceding_posts'][0]['author_name']
    archived1 = record['preceding_posts'][0]['archived']
    controversiality1 = record['preceding_posts'][0]['controversiality']
    if(record['preceding_posts'][0]['ups'] > 0):
        votes['upvotes'] += record['preceding_posts'][0]['ups']
    elif(record['preceding_posts'][0]['ups'] < 0):
        votes['downvotes'] += record['preceding_posts'][0]['ups']
    if(record['preceding_posts'][0]['delta'] == True):
        delta += 1
    convo2 = record['preceding_posts'][1]['body']
    author2 = record['preceding_posts'][1]['author_name']
    archived2 = record['preceding_posts'][1]['archived']
    controversiality2 = record['preceding_posts'][1]['controversiality']
    if(record['preceding_posts'][1]['ups'] > 0):
        votes['upvotes'] += record['preceding_posts'][1]['ups']
    elif(record['preceding_posts'][1]['ups'] < 0):
        votes['downvotes'] += record['preceding_posts'][1]['ups']
    if(record['preceding_posts'][1]['delta'] == True):
        delta += 1
    train_record.append([ID,convo1,convo2,author1,author2,
                votes['upvotes'],votes['downvotes'],archived1,archived2,
                controversiality1,controversiality2,delta,label])
train = train.append(train_record)
train.columns=['id','convo1','convo2','author1','author2','upvotes','downvotes',
               'archived1','archived2','controversiality1','controversiality2',
               'delta','label']

import spacy
from spacy.symbols import nsubj

nlp = spacy.load("en_core_web_sm")

def find_youphrases(txt):
    doc = nlp(txt)            
    you_phrases = []     
    for sent in doc.sents:       
        for possible_subject in sent:
            if possible_subject.dep == nsubj and possible_subject.head.dep_ == 'ROOT' and possible_subject.pos_ == 'PRON':
                if possible_subject.text.startswith('y'):
                    you = possible_subject.text 
                    childs = [child.text for child in possible_subject.head.children]
                    child_dep = dict.fromkeys(childs)
                    for child in possible_subject.head.children:
                        child_dep[child.text] = child.dep_
#                    print(child_dep)
                    auxl = ""
                    negtv = ""
                    for key in child_dep.keys():                    
                        if child_dep[key] == 'aux':
                            auxl = key
                        if child_dep[key] == 'neg':
                            negtv = key
                    if auxl != "":
                        if childs.index(auxl) < childs.index(you):
                            try:
                                if childs.index(negtv) < childs.index(you):
                                    if childs.index(auxl) < childs.index(negtv):
                                        if "'" in negtv:
#                                            print(auxl+negtv+" "+you)
                                            you_phrases.append(auxl+negtv+" "+you)
                                        else:
#                                            print(auxl+" "+negtv+" "+you)
                                            you_phrases.append(auxl+" "+negtv+" "+you)
                                elif childs.index(negtv) > childs.index(you):
                                    if childs.index(auxl) < childs.index(negtv):
                                        if "'" in negtv:
#                                            print(auxl+" "+you+negtv)
                                            you_phrases.append(auxl+" "+you+negtv)
                                        else:
#                                            print(auxl+" "+you+" "+negtv)
                                            you_phrases.append(auxl+" "+you+" "+negtv)
                            except ValueError: #no negative 
#                                print(auxl+" "+you)
                                you_phrases.append(auxl+" "+you)
                        elif childs.index(auxl) > childs.index(you):
                            try:
                                if childs.index(negtv) < childs.index(you):
                                    if auxl.startswith("'"):
#                                        print(negtv+" "+you+auxl)
                                        you_phrases.append(negtv+" "+you+auxl)
                                    else:
#                                        print(negtv+" "+you+" "+auxl)
                                        you_phrases.append(negtv+" "+you+" "+auxl)
                                elif childs.index(negtv) > childs.index(you):
                                    if childs.index(auxl) < childs.index(negtv):
                                        if "'" in negtv:
#                                            print(you+" "+auxl+negtv)
                                            you_phrases.append(you+" "+auxl+negtv)
                                        else:
                                            if auxl.startswith("'"):
#                                                print(you+auxl+" "+negtv)
                                                you_phrases.append(you+auxl+" "+negtv)
                                            else:
#                                                print(you+" "+auxl+" "+negtv)
                                                you_phrases.append(you+" "+auxl+" "+negtv)
                                    else:
#                                        print(you+" "+negtv+" "+auxl)
                                        you_phrases.append(you+" "+negtv+" "+auxl)
                            except ValueError: #no negative 
                                if auxl.startswith("'"):
#                                    print(you+auxl)
                                    you_phrases.append(you+auxl)
                                else:
#                                    print(you+" "+auxl)
                                    you_phrases.append(you+" "+auxl)
    return you_phrases

yous = set()
for convo in train[['convo1','convo2']]:
    convos = train[convo]
    for txt in convos:
#        print(txt.lower())
        yous.update(find_youphrases(txt.lower()))
 
AH_dict = dict.fromkeys(yous,0) 
nonAH_dict = dict.fromkeys(yous,0)   

for convo in train[['convo1','convo2']]:
    convos = train[convo]
    for i in range(len(convos)):
        text = convos[i].lower()
        for phrase in yous:
            if phrase in text:
                if train['label'][i] == 1:
                    AH_dict[phrase] += 1
                elif train['label'][i] == 0:
                    nonAH_dict[phrase] += 1
                    

AH_df = pd.DataFrame.from_dict(AH_dict, orient='index')
nonAH_df = pd.DataFrame.from_dict(nonAH_dict, orient='index')
youphrase_df_original = AH_df.merge(nonAH_df, left_index=True, right_index=True)
youphrase_df_original = youphrase_df_original.reset_index()
youphrase_df_original.columns = ['you phrases','AH count','non AH count']
youphrase_df = pd.melt(youphrase_df_original, id_vars="you phrases", var_name="Types", value_name="Occurences")
youphrase_df_original['relative diff'] = (youphrase_df_original['AH count'] - youphrase_df_original['non AH count']) / youphrase_df_original['non AH count']
AH_you_phrases = youphrase_df_original[youphrase_df_original['relative diff'] >= 0.25]['you phrases']
AH_you_phrases = list(AH_you_phrases)

import seaborn as sns
import matplotlib.pyplot as plt
ax = sns.catplot(x='you phrases', y='Occurences', hue='Types', data=youphrase_df, kind='bar', height=7, aspect=10)
plt.xticks(rotation=90)
#ax.set(xlabel='Categories', ylabel='Number of Arguments')
plt.show()